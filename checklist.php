<?php
/**
 * Plugin Name: Jupitercow Checklists
 * Plugin URI: http://jcow.com
 * Description: Create and use checklists. How type A of us!
 * Author: Ben Coy
 * Author URI: http://jcow.com
 */
class jcow_checklist {
	function __construct() {
		// set everything up
		add_action('admin_menu', array($this, 'menu_page'));
		add_action('init', array($this, 'checklist_post_types'));
		add_action('wp_enqueue_scripts', array($this, 'add_chrome'));
		add_action('admin_enqueue_scripts', array($this, 'add_chrome'));

		// ajax calls
		add_action( 'wp_ajax_jcow_get_definition', array($this, 'ajax_return_definition') );
		add_action( 'wp_ajax_jcow_save_definition', array($this, 'ajax_save_definition') );
		add_action( 'wp_ajax_jcow_new_instance', array($this, 'ajax_new_instance') );
		add_action( 'wp_ajax_jcow_save_instance', array($this, 'ajax_save_instance') );
		add_action( 'wp_ajax_jcow_revert_instance', array($this, 'ajax_revert_instance') );
		add_action( 'wp_ajax_jcow_delete_instance', array($this, 'ajax_delete_instance') );

		// handle front-end display
		add_filter( 'the_content', array($this, 'frontend_filter'), 1 );
	}

	/**
	 * we need to store both list types and list instances
	 */
	function checklist_post_types() {
		register_post_type('jcow_checklists',
			array(
				'labels' => array(
					'name' => 'Checklists Definitions',
					'singular_name' => 'Checklist Definition'
				),
				'public' => true,
				'show_ui' => false,
				'show_in_nav_menus' => false,
				'show_in_menu' => false,
				'exclude_from_search' => true,
				'has_archive' => true,
				'rewrite' => array('slug' => 'checklist_definition'),
				'supports' => array('title', 'editor', 'revisions')
			)
		);
		register_post_type('jcow_list_instances',
			array(
				'labels' => array(
					'name' => 'Checklists',
					'singular_name' => 'Checklist'
				),
				'public' => true,
				'show_ui' => false,
				'show_in_nav_menus' => false,
				'show_in_menu' => false,
				'exclude_from_search' => true,
				'has_archive' => true,
				'rewrite' => array('slug' => 'checklist'),
				'supports' => array('title', 'editor', 'revisions')
			)
		);
	}

	/**
	 * get array of definition posts
	 */
	function get_definitions() {
		return get_posts(
			array(
				'posts_per_page' => -1,
				'orderby' => 'title',
				'order' => 'ASC',
				'post_type' => 'jcow_checklists'
			)
		);
	}

	/**
	 * get array of instance posts for given definition
	 */
	function get_instances($def) {
		return get_posts(
			array(
				'posts_per_page' => -1,
				'orderby' => 'title',
				'order' => 'ASC',
				'post_type' => 'jcow_list_instances',
				'meta_key' => 'jcow_list_definition',
				'meta_value' => $def
			)
		);
	}


	/**
	 * include script and styles
	 */
	function add_chrome() {
		wp_register_script( 'jcow_checklists_js', plugins_url('checklists.js', __FILE__), array('jquery') );
		wp_localize_script( 'jcow_checklists_js', 'jcow_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php')));
		wp_enqueue_script( 'jcow_checklists_js' );
		wp_enqueue_script( 'jcow_sortable_js', plugins_url('jquery.sortable.min.js', __FILE__), array('jquery') );
		wp_enqueue_style( 'jcow_checklists_css', plugins_url('checklists.css', __FILE__) );
	}

	/**
	 * create the panel on the admin side
	 */
	function menu_page() {
		add_menu_page('Jupitercow Checklists', 'Checklists', 'read', 'jcow_checklists', array($this, 'admin_panel'));
	}
	function admin_panel() {
?>
		<div id="checklists-wrap">
			<ul id="checklists-nav">
				<?php
					$lists = $this->get_definitions();
					foreach ($lists as $list) {
						echo "<li data-post=\"{$list->ID}\">{$list->post_title}</li>";
					}
				?>
				<li data-post="new" class="dashicons dashicons-plus"></li>
			</ul>
			<form id="checklists-detail" action="" method="post" onsubmit="return false;"></form>
			<div id="checklists-instances">
				<h2>List Instances</h2>
				<table>
				</table>
			</div>
		</div>
<?php
	}


	/**
	 * AJAX supply definition to front end
	 */
	function ajax_return_definition() {
		if ($post = get_post($_POST['post_id'])) {
			$instances_res = $this->get_instances($post->ID);
			$instances = array();
			foreach ($instances_res as $instance) {
				$author = get_userdata($instance->post_author);
				$instances[$instance->ID] = array(
					'title' => $instance->post_title,
					'author'=> $author->display_name,
					'permalink'=> get_permalink($instance->ID)
				);
			}
			echo json_encode(
				array(
					'title' => $post->post_title,
					'permalink' => get_permalink($post->ID),
					'data' => unserialize($post->post_content),
					'instances' => $instances
				)
			);
		} else {
			echo json_encode(
				array(	'title' => 'New Checklist',
						'permalink' => '',
						'data' => array(),
						'instances' => array()
				)
			);
		}
		die();
	}

	/**
	 * AJAX save checklist definition
	 */
	function ajax_save_definition() {
		if (
			!empty($_POST['post_id']) &&
			!empty($_POST['title'])
		) {
			if ( !empty($_POST['data']) ) {
				echo wp_insert_post( array(
					'ID' => (('new' == $_POST['post_id']) ? '' : $_POST['post_id']),
					'post_type' => 'jcow_checklists',
					'post_status' => 'publish',
					'post_content' => serialize($_POST['data']),
					'post_title' => $_POST['title'],
					'post_name' => sanitize_title($_POST['title'])
				));
			} else {
				wp_trash_post( $_POST['post_id'] );
				echo '0';
			}
		}
		die();
	}

	/**
	 * AJAX new checklist instance
	 */
	function ajax_new_instance() {
		if (
			!empty($_POST['definition']) &&
			!empty($_POST['title'])
		) {
			$definition = get_post($_POST['definition']);
			$list = unserialize($definition->post_content);
			foreach ($list as &$v) {
				$v = false;
			}
			$newPost = wp_insert_post( array(
				'post_type' => 'jcow_list_instances',
				'post_status' => 'publish',
				'post_content' => serialize($list),
				'post_title' => $_POST['title'],
				'post_name' => sanitize_title($_POST['title'])
			));
			update_post_meta($newPost, 'jcow_list_definition', $_POST['definition']);
			echo get_permalink($newPost);
		}
		die();
	}

	/**
	 * AJAX new checklist instance
	 */
	function ajax_save_instance() {
		if (
			!empty($_POST['instance']) &&
			!empty($_POST['checks'])
		) {
			// rather than creating a new content string, we edit the 
			// one we have to guarantee that the order does not change, etc
			$instance = get_post($_POST['instance']);
			$content = unserialize($instance->post_content);
			foreach ($_POST['checks'] as $k => $v) {
				if (isset($content[$k])) {
					$content[$k] = ((!$v || strtolower($v) == 'false') ? false : true);
				}
			}
			echo wp_update_post( array(
				'ID' => $_POST['instance'],
				'post_content' => serialize($content)
			));
		}
		die();
	}

	/**
	 * AJAX revert checklist instance
	 */
	function ajax_revert_instance() {
		if (!empty($_POST['instance'])) {
			if (
				($old = get_post($_POST['instance'])) &&
				($definition = get_post(get_post_meta($_POST['instance'], 'jcow_list_definition', true)))
			) {
				$data = unserialize($old->post_content);
				$content = unserialize($definition->post_content);
				foreach ($content as $k => &$v) {
					if (!empty($data[$k])) {
						$v = $data[$k];
					} else {
						$v = false;
					}
				}
				wp_update_post( array(
					'ID' => $_POST['instance'],
					'post_content' => serialize($content)
				));
				echo get_permalink($_POST['instance']);
			}
		}
		die();
	}

	/**
	 * AJAX delete checklist instance
	 */
	function ajax_delete_instance() {
		if (!empty($_POST['instance'])) {
			wp_trash_post($_POST['instance']);
			echo $_POST['instance'];
		}
		die();
	}

	/**
	 * front-end display of both definitions and instances
	 */
	function frontend_filter($content) {
		global $post;

		if ('jcow_checklists' == $post->post_type) {

			ob_start();

			echo '<ul id="jcow_checklists">';
			$instances = $this->get_instances($post->ID);
			foreach ($instances as $instance) {
				echo '<li><a href="'.get_permalink($instance->ID).'">'.$instance->post_title.'</a></li>';
			}
			echo '</ul>';

			echo '<button data-definition="'.$post->ID.'" id="jcowNewInstance">New List</button>';

			return str_replace("\n", '', ob_get_clean());

		} elseif ('jcow_list_instances' == $post->post_type) {

			$definition = get_post(get_post_meta($post->ID, 'jcow_list_definition', true));
			$checklist = unserialize($definition->post_content);

			ob_start();
			echo '<form action="" method="post" onsubmit="return false" id="jcow_checklist_instances">';
			echo '<ol>';
			$items = unserialize($post->post_content);
			foreach ($items as $k => $item) {
?>
				<li>
					<input type="checkbox"
						id="item_<?php echo $k; ?>"
						name="items[<?php echo $k; ?>]"
						<?php if ($item) echo 'checked="true"'; ?>
					>
					<label for="item_<?php echo $k; ?>">
					<?php
						if (isset($checklist[$k])) {
							echo $checklist[$k];
						} else {
							echo '[deleted item]';
						}
					?>
					</label>
				</li>
<?php
			}
			echo '</ol>';
			echo '<button id="jcowSaveInstance" data-id="'.$post->ID.'">Save</button>';
			echo '</form>';
			return str_replace("\n", '', ob_get_clean());

		} else return $content;
	}

}

if (empty($jcow_checklist)) {
	$jcow_checklist = new jcow_checklist;
}
