jQuery(document).ready(function($) {

	// in admin area, select the list definition, load data from server
	$('#checklists-nav li').click(loadChecklistDefinition);
	function loadChecklistDefinition() {
		var $me = $(this);

		$.post(
			jcow_ajax.ajaxurl, 
			{
				'action': 'jcow_get_definition',
				'post_id': $me.data('post')
			},
			function(response) {
				if (response) {
					var listdata = JSON.parse(response);
					if (listdata &&		
						('undefined' != typeof listdata.title) && 
						('undefined' != typeof listdata.data)
					) {

						$('#checklists-nav li').removeClass('selected');
						$me.addClass('selected');
						$me.parents('ul').attr('data-active', $me.data('post'));

						var $container = $('#checklists-detail');
						$container.empty();

						var h1 = document.createElement('h1');
						h1.appendChild(document.createElement('input'));
						h1.lastChild.setAttribute('name', 'title');
						h1.lastChild.setAttribute('type', 'text');
						h1.lastChild.setAttribute('value', listdata.title);
						$container.append(h1);

						var max = 0;
						var list = document.createElement('ul');
						var $list = $(list);
						for (var i in listdata.data) {
							addNewChecklistItem($list, i, listdata.data[i]);
						}
						addNewChecklistItem($list);

						$container.append(list);
						$list.sortable();

						var submit = document.createElement('button');
						submit.appendChild(document.createTextNode('Save Checklist'));
						$container.append(submit);
						// fwd checklist definition to server for saving
						$(submit).click( function() {
							var postdata = {
										'action': 'jcow_save_definition',
										'post_id': $me.data('post'),
										'title': $('h1 input[name="title"]').val(),
										'data': {}
									};
							$container.find('ul input').each(function() {
								if (this.value) {
									postdata.data['jcow_'+this.getAttribute('data-id')] = this.value;
								}
							});
							if (0 == postdata.data.length) {
								alert('Lists must have at least one item!');
							} else {
								$.post(
									jcow_ajax.ajaxurl, 
									postdata,
									function(answer) {
										if (parseInt(answer) > 0) {
											if ('new' == postdata.post_id) {
												var li = document.createElement('li');
												li.setAttribute('data-post', answer);
												li.className = 'selected';
												li.appendChild(document.createTextNode(postdata.title));
												$(li).click(loadChecklistDefinition);
												$('#checklists-nav li').removeClass('selected');
												$('#checklists-nav li:last-child').before(li);
												$(li).trigger('click');
											} else {
												$me.empty();
												$me.append(document.createTextNode(postdata.title));
											}
											alert('Checklist saved');
										} else {
											alert('There was an error saving this checklist');
										}
									}
								);
							}
						});

						if ('new' != $me.data('post')) {
							var del = document.createElement('button');
							del.appendChild(document.createTextNode('Delete Checklist'));
							$container.append(del);
							$(del).click( function() {
								var postdata = {
											'action': 'jcow_save_definition',
											'post_id': $me.data('post'),
											'title': $('h1 input[name="title"]').val(),
											'data': {}
										};
								$.post(
									jcow_ajax.ajaxurl, 
									postdata,
									function(answer) {
										$me.remove();
										$container.empty();
										alert('Checklist deleted');
									}
								);
							});
						}

						/*
							if (listdata.permalink) {
								var instances = document.createElement('button');
								instances.className = 'viewInstances';
								instances.appendChild(document.createTextNode('View Lists'));
								$container.append(instances);
								$(instances).click(function() {
									document.location.href = listdata.permalink;
								});
							}
						*/

						$instancesContainer = $('#checklists-wrap #checklists-instances table');
						$instancesContainer.empty();
						if (listdata.instances) {
							for (var i in listdata.instances) {
								var tr = document.createElement('tr');
								tr.className = 'instance';
								tr.setAttribute('data-id', i);

								tr.appendChild(document.createElement('td')); // link
								tr.lastChild.appendChild(document.createElement('a'));
								tr.lastChild.lastChild.setAttribute('href', listdata.instances[i].permalink);
								tr.lastChild.lastChild.appendChild(document.createTextNode(listdata.instances[i].title));

								tr.appendChild(document.createElement('td')); // author
								tr.lastChild.appendChild(document.createTextNode(listdata.instances[i].author));

								tr.appendChild(document.createElement('td')); // reset
								tr.lastChild.appendChild(document.createElement('span'));
								tr.lastChild.lastChild.className = "dashicons dashicons-migrate";
								tr.lastChild.lastChild.setAttribute('title', 'Update to definition');
								
								tr.appendChild(document.createElement('td')); // delete
								tr.lastChild.appendChild(document.createElement('span'));
								tr.lastChild.lastChild.className = "dashicons dashicons-trash";
								tr.lastChild.lastChild.setAttribute('title', 'Delete list instance');

								$instancesContainer.append(tr);
							}
						}

						var tr = document.createElement('tr');
						tr.appendChild(document.createElement('td'));
						tr.lastChild.setAttribute('colspan', '4');
						tr.lastChild.appendChild(document.createElement('button'));
						tr.lastChild.lastChild.appendChild(document.createTextNode('New instance'));
						$(tr.lastChild.lastChild).click(function() {
							var newTitle = prompt('List title', 'New ' + listdata.title);
							$.post(
								jcow_ajax.ajaxurl, 
								{
									'action': 'jcow_new_instance',
									'definition': $me.data('post'),
									'title': newTitle
								},
								function(response) {
									if (response) {
										document.location.href = response;
									}
								}
							);
						});
						$instancesContainer.append(tr);

						$instancesContainer.find('.dashicons-migrate').click(function() {
							$.post(
								jcow_ajax.ajaxurl, 
								{
									'action': 'jcow_revert_instance',
									'instance': this.parentNode.parentNode.getAttribute('data-id')
								},
								function(response) {
									if (response) {
										document.location.href = response;
									}
								}
							);
						});
						$instancesContainer.find('.dashicons-trash').click(function() {
							if (confirm('Really delete list? There is no undo.')) {
								$tr = $(this).parents('tr').first();
								$.post(
									jcow_ajax.ajaxurl, 
									{
										'action': 'jcow_delete_instance',
										'instance': $tr.data('id')
									},
									function(response) {
										if (response) {
											$tr.remove();
										}
									}
								);
							}
						});
					}
				}
			}
		);
	}

	// create new list definition
	function addNewChecklistItem($ul, id, val) {
		if (!$ul) {
			var $ul = $('#checklists-detail ul');
		}

		if (!id) {
			var id = 0;
			$ul.find('input').each( function () {
				id = Math.max(id, parseInt(this.getAttribute('data-id')));
			});
			id++;
		} else id = id.replace(/\D/g, '');

		if (!val) {
			var val = '';
		}

		var li = document.createElement('li');
		li.appendChild(document.createElement('input'));
		li.lastChild.setAttribute('type', 'text');
		li.lastChild.setAttribute('data-id', id);

		if (val) {
			li.lastChild.value = val;
		} else {
			$(li.lastChild).change(function() {
				$(this).off('change');
				addNewChecklistItem($ul);
			});
		}

		$ul.append(li);
		$ul.sortable();
	}

	// from definition post on front end
	// on request to create new instance, fwd request to server, then route to new page
	$('#jcowNewInstance').click( function() {
		$me = $(this);
		var newTitle = prompt('List title', 'New '+$('h1.entry-title').text().trim());
		$.post(
			jcow_ajax.ajaxurl, 
			{
				'action': 'jcow_new_instance',
				'definition': $me.data('definition'),
				'title': newTitle
			},
			function(response) {
				if (response) {
					document.location.href = response;
				}
			}
		);

	});

	// post checkbox data to server to save
	$('#jcowSaveInstance').click( function() {
		$me = $(this);
		var checks = {};
		$me.parents('form').find('input[type="checkbox"]').each( function() {
			checks[this.getAttribute('name').match(/\[(.*?)\]/)[1]] = this.checked;
		});
		$.post(
			jcow_ajax.ajaxurl, 
			{
				'action': 'jcow_save_instance',
				'instance': $me.data('id'),
				'checks': checks
			},
			function(response) {
				if (response) {
					alert('List saved.');
				} else {
					alert('Error saving. Please try again.');
				}
			}
		);
	});

});
